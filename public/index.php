<?php
session_start();

define('ROOT_DIR', dirname(__DIR__) . DIRECTORY_SEPARATOR);
define('APP_DIR', ROOT_DIR . 'App' . DIRECTORY_SEPARATOR);
define('CONTROLLERS_DIR', APP_DIR . 'Controllers' . DIRECTORY_SEPARATOR);
define('VIEWS_DIR', APP_DIR . 'Views' . DIRECTORY_SEPARATOR);

$dirs = [ROOT_DIR, APP_DIR, CONTROLLERS_DIR];

set_include_path(get_include_path() . PATH_SEPARATOR . implode(PATH_SEPARATOR, $dirs));
spl_autoload_register('spl_autoload', false);

require_once APP_DIR.'App.php';

new \App\App();