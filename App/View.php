<?php

namespace App;

class View
{
    const VIEW_EXTENSION = '.phtml';

    protected $data;
    protected $file;

    public function __construct($file, $data = array())
    {
        $this->file = $file;
        $this->data = $data;
    }

    public function render()
    {
        $view = VIEWS_DIR . $this->file . self::VIEW_EXTENSION;

        if (file_exists($view)) {
            $this->populateData($_SESSION);
            extract($this->data);

            include $view;
        }
    }

    public static function formatViewPath($name)
    {
        return str_replace('.', DIRECTORY_SEPARATOR, $name);
    }

    private function populateData($data){
        if (is_array($data)){
            foreach ($data as $key => $datum) {
                $this->data[$key] = $datum;
            }
        }
    }
}