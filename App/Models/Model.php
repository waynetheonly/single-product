<?php

namespace App\Models;

use PDO;
use App\Db;

class Model extends Db
{
    protected $table;

    public function get($start, $limit)
    {
        $query = "SELECT * FROM ".$this->table." ORDER BY date_add DESC LIMIT ".$limit;

        if($start > 0){
            $query .= " OFFSET ".$start;
        }

        $db = $this->connect()->query($query);

        $data = $db->fetchAll(PDO::FETCH_ASSOC);

        return $data;
    }

    public function getAll()
    {
        $db = $this->connect()->query("SELECT * FROM " . $this->table . " ORDER BY 'date_add'");

        $data = $db->fetchAll(PDO::FETCH_ASSOC);

        return $data;
    }

    public function getById($id)
    {
        $data = array();

        $db = $this->connect()->prepare("SELECT * FROM " . $this->table . " WHERE id = ?");
        if ($db->execute([$id])) {
            $data = $db->fetch(PDO::FETCH_ASSOC);
        }

        return $data;
    }

    public function save()
    {
        $db = $this->connect()->prepare("INSERT INTO orders (first_name, last_name, email, phone, address) VALUES (?,?,?,?,?)");

        return $db->execute([$this->first_name, $this->last_name, $this->email, $this->phone, $this->address]);
    }
}