<?php

namespace App;

class App
{
    protected $controller = 'IndexController';
    protected $action = 'index';
    protected $params = array();

    public function __construct()
    {
        $this->parseUrl();

        if (file_exists(CONTROLLERS_DIR . $this->controller . '.php')) {
            $controller = 'App' . DIRECTORY_SEPARATOR . 'Controllers' . DIRECTORY_SEPARATOR . $this->controller;

            $this->controller = new $controller;

            if (method_exists($this->controller, $this->action)) {
                call_user_func_array([$this->controller, $this->action], [$this->params]);
            }
        } else {
            $view = new View('404');

            return $view->render();
        }

        Session::resetAllData();
    }

    protected function parseUrl()
    {

        $request = trim($_SERVER['REQUEST_URI'], '/');
        if (!empty($request)) {
            $url = explode('/', $request);
            $this->controller = isset($url[0]) ? ucfirst($url[0]) . 'Controller' : 'IndexController';

            if (file_exists(CONTROLLERS_DIR . $this->controller . '.php')) {
                $controller = 'App' . DIRECTORY_SEPARATOR . 'Controllers' . DIRECTORY_SEPARATOR . $this->controller;
                $controller = new $controller;

                if (isset($url[1]) && method_exists($controller, $url[1])) {
                    $this->action = $url[1];
                    unset($url[0], $url[1]);
                } else {
                    unset($url[0]);
                }
            }

            $this->params = !empty($url) ? array_values($url) : array();
        }
    }
}