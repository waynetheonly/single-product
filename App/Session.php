<?php

namespace App;

class Session
{
    public static function setErrors($errors)
    {
        foreach ($errors as $key => $error) {
            $_SESSION['errors'][$key] = $error;
        }
    }

    public static function setMessage($key, $message)
    {
        $_SESSION['messages'][$key] = $message;
    }

    public static function setVar($var, $data)
    {
        $_SESSION[$var] = $data;
    }

    public static function resetAllData()
    {
        unset($_SESSION['errors'], $_SESSION['messages'], $_SESSION['old']);
    }
}