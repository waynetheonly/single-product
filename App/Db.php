<?php

namespace App;

use PDO;
use Exception;

class Db
{
    const DB_HOST = 'localhost';
    const DB_USER = 'root';
    const DB_PASSWORD = '';
    const DB_NAME = 'single_product';
    const DB_CHARSET = 'utf8mb4';

    protected function connect()
    {
        try {
            $dsn = 'mysql:host=' . self::DB_HOST . ';dbname=' . self::DB_NAME . ';charset' . self::DB_CHARSET;
            $pdo = new PDO($dsn, self::DB_USER, self::DB_PASSWORD);

            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            return $pdo;
        } catch (Exception $e) {
            echo 'Connection failed: ' . $e->getMessage();
            die;
        }
    }
}