<?php

namespace App\Controllers;

use App\Models\Product;

class IndexController extends Controller
{
    public function index()
    {
        $product = new Product();

        $this->view('product.index', ['product' => $product->getById(1)]);
    }
}