<?php

namespace App\Controllers;

use App\Session;
use App\Models\Order;

class OrdersController extends Controller
{
    private $limit = 10;
    private $page = 1;
    private $url = '/orders/';

    public function index($params = array())
    {
        $order = new Order();
        $numOrders = count($order->getAll());
        $pages = ceil($numOrders / $this->limit);

        $this->validatePage($params);

        $start = ($this->page - 1) * $this->limit;
        $orders = $order->get($start, $this->limit);

        $data = array(
            'orders' => $orders,
            'pages' => (int)$pages,
            'currentPage' => (int)$this->page,
            'url' => $this->url
        );

        $this->view('order.index', $data);
    }

    public function store()
    {
        if (!isset($_SERVER['HTTP_REFERER'])) {
            $this->redirectHome();
        }

        $request = $this->validateRequest($_REQUEST);

        $isValidData = $this->validateOrderData($request);

        if ($isValidData) {
            $order = new Order();

            $order->first_name = $request['firstName'];
            $order->last_name = $request['lastName'];
            $order->email = $request['email'];
            $order->phone = $request['phone'];
            $order->address = $request['address'];

            if ($order->save()) {
                Session::setMessage('order_success', 'Successfully ordered a product!');
            } else {
                Session::setErrors(array('Could not complete order.'));
            }
        }

        $this->redirectHome();
    }

    private function validateOrderData($data)
    {
        if (empty($data['firstName'])) {
            $this->errors['firstName'] = 'First name is required.';
        }

        if (empty($data['lastName'])) {
            $this->errors['lastName'] = 'Last name is required.';
        }

        if (empty($data['email']) || !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $this->errors['email'] = ' Please enter a valid email address.';
        }

        if (!isset($data['address']) || empty($data['address'])) {
            $this->errors['address'] = 'Address is required.';
        }

        Session::setErrors($this->errors);

        if (!empty($this->errors)) {
            Session::setVar('old', $data);

            $this->redirectHome();
        }

        return true;
    }

    private function validatePage($params)
    {
        if (count($params) <= 1) {
            return false;
        }

        if (!isset($params[0]) || $params[0] !== 'page') {
            return false;
        }

        if (!isset($params[1]) || !is_numeric($params[1])) {
            return false;
        }

        $this->page = $params[1];

        return true;
    }
}