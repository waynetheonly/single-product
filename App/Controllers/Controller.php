<?php

namespace App\Controllers;

use App\View;

class Controller
{
    protected $view;
    protected $errors = array();

    public function view($name, $data)
    {
        $path = View::formatViewPath($name);
        $this->view = new View($path, $data);

        return $this->view->render();
    }

    protected function validateRequest($data)
    {
        $result = array();

        foreach ($data as $key => $datum) {
            $result[$key] = urldecode(html_entity_decode(strip_tags($datum)));
        }

        return $result;
    }

    protected function redirectHome(){
        header("Location: /");
        exit;
    }
}